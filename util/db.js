// (选择模式1. readonly: 只读；2. readwrite: 可读可写；3. versionchange: 数据库版本变化)
const dbStoreName = 'fruit'

// 写入数据
export const writeData = (db, dataList = []) => {
  return new Promise((resolve,reject)=>{
    const transaction = db.transaction([dbStoreName], 'readwrite')
    const objectStore = transaction.objectStore(dbStoreName)
    dataList.forEach(async data => {
      objectStore.add(data)
    })
    transaction.oncomplete = () => {
      resolve({status: 200})
    }
    transaction.onerror = function(event) {
      console.log('Transaction failed:', event.target.errorCode);
      reject({status: event.target.errorCode})
    };
  })
}


export const readAll = db => {
  return new Promise((resolve,reject)=>{
    const transaction = db.transaction([dbStoreName])
    const objectStore = transaction.objectStore(dbStoreName)
    const result = []
    // 使用流标 openCursor
    objectStore.openCursor().onsuccess = function (e) {
      const cursor = e.target.result
      if (cursor) {
        result.push(cursor.value)
        cursor.continue()
      } else {
        resolve(result)
      }
    }
    objectStore.onerror = function(event) {
      reject(event.target.errorCode);
    };
  })
}

// 模糊查询
export const fuzzySearch = (db, indexName, searchTerm) => {
  return new Promise((resolve,reject)=>{
    const index = db.transaction([dbStoreName],'readonly').objectStore(dbStoreName).index(indexName)
    const result = []
    index.openCursor().onsuccess = e=>{
      const cursor = e.target.result
      if (cursor) {
        if(cursor.value[indexName].includes(searchTerm)) {
          result.push(cursor.value)
        }
        cursor.continue()
      } else {
        resolve(result)
      }
    }
    index.onerror = function(event) {
      reject(event.target.errorCode);
    };
  })
}

export const readData = (db, id) => {
  return new Promise((resolve,reject)=>{
    const request = db.transaction([dbStoreName],'readonly').objectStore(dbStoreName).get(+id)
    request.onerror = function (event) {
      console.log('事务失败')
      reject(event)
    }

    request.onsuccess = function (event) {
      console.log(event);
      if (request.result) {
        resolve(request.result)
      } else {
        console.log('未获得数据记录')
        resolve(null)
      }
    }

  })
}

export const monthSearch = (db, searchMonth) => {
  return new Promise((resolve,reject) => {
    const index = db.transaction([dbStoreName],'readonly').objectStore(dbStoreName).index('ripeningTime')
    const result = []
    index.openCursor().onsuccess = e=>{
      const cursor = e.target.result
      if (cursor) {
        if(cursor.value.ripeningTime.includes(+searchMonth)) {
          result.push(cursor.value)
        }
        cursor.continue()
      } else {
        resolve(result)
      }
    }
    index.onerror = function(event) {
      reject(event.target.errorCode);
    };
  })
}

export const initDb = () => {
  return new Promise((resolve,reject)=>{
    const request = indexedDB.open('my-db')
    let db = null
    let hasUpdate = false
    request.onerror = function (event) {
      console.log('数据库打开报错', event)
    }
    request.onsuccess = async function (event) {
      db = event.target.result
      console.log('数据库打开成功')
      resolve({db, hasUpdate})
    }
    request.onupgradeneeded = function (event) {
      hasUpdate = true
      console.log('版本升级')
      db = event.target.result
      let objectStore = null
      // 创建对象仓库
      if (!db.objectStoreNames.contains(dbStoreName)) {
        objectStore = db.createObjectStore(dbStoreName, {
          keyPath: 'id',
          autoIncrement: true
        })
      }
      // 创建索引 便于搜索
      objectStore.createIndex('name', 'name', { unique: false })
      objectStore.createIndex('ripeningTime', 'ripeningTime', { unique: false })
    }
    request.onerror = function(e) {
      console.log('数据库创建失败');
      reject(e.target.errorCode)
    }
  })
}
